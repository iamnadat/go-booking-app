package helper

import (
	"fmt"
)

func GetUserInput() (string, string, uint) {
	// define username as string type
	var userName string
	var email string
	var userTickets uint

	fmt.Println("What is your username")
	// the & is a pointer to the variable userName
	fmt.Scan(&userName)
	fmt.Printf("Hello %v\n", userName)

	fmt.Println("What is your email")
	fmt.Scan(&email)

	fmt.Println("how may tickets do you want")
	fmt.Scan(&userTickets)

	return userName, email, userTickets
}
