package main

import (
	"fmt"
	"go-booking-app/helper"
)

// package level variables
//  package level variables have to be declared in long form
const conferenceName = "Best Go Conference"

// unit makes sure the value cant go below 0
const conferenTickets uint = 50

var remainingTickets uint = 50

// SLICE
var bookings []string

func main() {

	// find the type of variable using %T
	// fmt.Printf("ConferenceTicket is %T\n", conferenTickets)

	greetUser()

	// infitnie loop
	for {
		userName, email, userTickets := helper.GetUserInput()
		// check if user tickets is more than remaining tickets
		if userTickets > remainingTickets {
			fmt.Printf("we only have %v tickets remianig, you cant book %v tickets\n", remainingTickets, userTickets)
			// dont break the loop restart the loop
			continue
		}

		remainingTickets = calcRemainingTickets(remainingTickets, userTickets)

		fmt.Printf("Thank You %v for booking %v tickets. you will get an email at %v\n", userName, userTickets, email)
		fmt.Printf("%v tickets left\n", remainingTickets)

		bookings = append(bookings, userName)
		fmt.Printf("These are all the bookings: %v\n", bookings)

		// check to see if there are any tickets left
		if remainingTickets == 0 {
			fmt.Println("confgerence is bookec out")
			break
		}

	}

}

func greetUser() {
	fmt.Println("Welcome to our'", conferenceName, "'booking application")
	fmt.Println("Book your ticket using this app")
	fmt.Println("There are", conferenTickets, " and", remainingTickets, "remaining!")
}

// have to specify the return type
func calcRemainingTickets(remainingTickets uint, userTickets uint) uint {
	return remainingTickets - userTickets
}

// create a variable of length 50 and add one item to the aray
// var bookings = [50]string{"Muhammad"}

// ARRAYS
// create an empty array of size 50
// var bookings [50]string

// bookings = append(bookings, "muhammad")
// fmt.Printf("the whole slice %v\n", bookings)
// fmt.Printf("first index of slice %v\n", bookings[0])
// fmt.Printf("bookings is %T\n", bookings)
// fmt.Printf("slice size is %v\n", len(bookings))

// For i loop

// loop through bookings and split on space to print out
// the _ is used to let go know we are not going to be using the index varaiable
// for _, booking := range bookings {
// 	var names = strings.Fields(booking)
// 	var first = names[0]
// 	fmt.Printf("first is: %v\n", first)
// }
